import cron from 'node-cron'
import { syncAllLandLicenseFromBlockchain } from '../Utils/getLandLicense.js'

const cronExpression = process.env.SYNC_LAND_LICENSE_CRON || '*/5 * * * *';
const syncLandLicenceJob = cron.schedule(cronExpression, async () => {
    try {
        // TODO: get all landLicense from DB
        // Get status landLicense from Blockchain
        // Update to DB
        await syncAllLandLicenseFromBlockchain();

    } catch (error) {
        console.error('Error occurred during cron job:', error);
        return res.status(500).json({ error: 'Error occurred during cron job' });
    }
});

export { syncLandLicenceJob }