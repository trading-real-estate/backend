import { getRole } from '../Utils/getRole.js'
import User from "../Models/userModel.js";


const getListNotary = (async (req, res) => {
    try {
        const users = await User.find()
        const filteredUsers = []
        const notaryUsers = []

        for (const user of users) {
            const role = await getRole(user.wallet_address)
            if (role !== 'ADMIN') {
                if (role === 'NOTARY') {
                    notaryUsers.push(user);
                } else {
                    filteredUsers.push(user);
                }
            }
        }
        res.json({
            users: filteredUsers,
            notaryUsers: notaryUsers
        });
    } catch (error) {
        console.log('Get list user failed', error.message);
        res.status(500).json({ message: 'Get list user failed' })
    }
})

export { getListNotary }