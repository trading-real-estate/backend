import LandTrade from "../Models/landTradeModel.js";

const searchProperties = async (req, res) => {
  try {
    const { query } = req.query;
    const searchCriteria = JSON.parse(query);
    const filter = [];
    if (searchCriteria.keyword) {
      filter.push({
        $or: [
          { title: { $regex: searchCriteria.keyword, $options: "i" } },
          { describe: { $regex: searchCriteria.keyword, $options: "i" } },
          { addressLand: { $regex: searchCriteria.keyword, $options: "i" } },
        ],
      });
    }
    if (searchCriteria.addressLand) {
      filter.push({
        addressLand: { $regex: searchCriteria.addressLand, $options: "i" },
      });
    }
    if (searchCriteria.area) {
      filter.push({
        area: { $gte: searchCriteria.area.min, $lte: searchCriteria.area.max },
      });
    }
    if (searchCriteria.price) {
      filter.push({
        price: {
          $gte: searchCriteria.price.min,
          $lte: searchCriteria.price.max,
        },
      });
    }
    const queryFilter = filter.length > 0 ? { $and: filter } : {};
    const result = await LandTrade.find(queryFilter);
    if (result.length === 0) {
      return res.status(404).json({ message: "No results found" });
    }
    res.json(result);
  } catch (error) {
    console.error("Search failed", error.message);
    res.status(500).json({ message: "Search failed", error: error.message });
  }
};

export { searchProperties };
