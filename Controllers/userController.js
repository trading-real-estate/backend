import LandLicence from "../Models/landLicenceModel.js";
import Otp from "../Models/otpModel.js";
import User from "../Models/userModel.js";
import { generateOTP, sendOTP, verifyOTP } from "../Utils/generateOTP.js";
import { getLandLicenseStatus } from "../Utils/getLandLicense.js";

//Send Email and Verify code
const sendEmailOTP = async (req, res) => {
  try {
    const { email } = req.body;
    const userId = req.user._id;
    //save email user
    await User.findByIdAndUpdate(userId, { email: email });
    //generate Otp
    const otp = generateOTP();
    // save  OTP  in  the database
    const otpData = new Otp({
      userId: userId,
      code: otp,
      createTime: new Date(),
      expiredTime: new Date(Date.now() + 5 * 60 * 1000), // Set OTP expiration time to 5 minutes from now
    });
    await otpData.save();
    // Send OTP  via email
    await sendOTP(email, otp);
    res.status(200).json({ message: "OTP sent successfully" });
  } catch (error) {
    console.log("Error sending email OTP:", error.message);
    res.status(500).json({ error: "Failed to send email OTP" });
  }
};

//POST verify email
const verifyEmailOTP = async (req, res) => {
  try {
    const { otp } = req.body;
    //verify OTP
    await verifyOTP(req.user._id, otp);
    res.status(200).json({ message: "Email OTP verified successfully" });
  } catch (error) {
    console.error("Error verifying email OTP:", error);
    res.status(500).json({ error: "Failed to verify email OTP" });
  }
};

// Get user KYC
const getAllUser = async (req, res) => {
  try {
    const userId = req.user._id; // Lấy ID người dùng từ middleware protect
    const user = await User.findById(userId).select(
      "wallet_address full_name id_card birthday hometown permanent_address idCard_front_url idCard_back_url email verifiedStatus"
    );
    if (user) {
      const {
        wallet_address,
        full_name,
        id_card,
        birthday,
        hometown,
        permanent_address,
        idCard_front_url,
        idCard_back_url,
        email,
        verifiedStatus,
      } = user;
      res.status(200).json({
        wallet_address,
        full_name,
        id_card,
        birthday,
        hometown,
        permanent_address,
        idCard_front_url,
        idCard_back_url,
        email,
        verifiedStatus,
      });
    } else {
      res.status(404).json({ message: "Không tìm thấy người dùng." });
    }
  } catch (error) {
    console.log("Error while get user information:", error.message);
    res.status(500).json({ error: "Error while get user information" });
  }
};

//Put : User account  KYC
const putUserKYC = async (req, res) => {
  try {
    const {
      full_name,
      id_card,
      birthday,
      hometown,
      permanent_address,
      idCard_front_url,
      idCard_back_url,
    } = req.body;
    const userId = req.user._id;
    const user = await User.findById(userId);
    const verifiedEmail = user.verifiedEmail;

    if (!verifiedEmail) {
      return res.status(400).json({ message: "Invalid OTP" });
    } else {
      await User.findByIdAndUpdate(userId, {
        full_name: full_name,
        id_card: id_card,
        birthday: birthday,
        hometown: hometown,
        permanent_address: permanent_address,
        idCard_front_url: idCard_front_url,
        idCard_back_url: idCard_back_url,
        verifiedStatus: "submitted KYC",
      });
      res.status(200).json({ message: "User KYC submitted successfully" });
    }
  } catch (error) {
    console.log("failed userKYC", error);
    res
      .status(500)
      .json({ message: "An error occurred while processing User KYC" });
  }
};

const getMyAsset = async (req, res) => {
  try {
    const userId = req.user._id;
    const user = await User.findById(userId);
    if (!user) {
      return res.status(400).json({ message: "User does not exist" });
    }
    const myAssets = await LandLicence.find({ userId: userId })
        .populate([
          {
            path : 'land_lot',
            select: 'land_plot_no map_sheet_no address common_land_area land_area_private spend_time purpose_of_using_land origin_of_land_use',
          },
            {
                path: 'house_id',
                select: 'construction_area floor_area level structure floors_of_number ownership_period year_complete_construction',
            },
            {
                path: 'other_construction_works',
                select: 'type_of_construction project_category construction_area floor_area ownership_period',
                options: { retainNullValues: true }
            },
            {
                path: 'prod_forest_is_an_artificial_forest',
                select: 'forest_type acreage_forest',
            },
            {
                path: 'perennial_trees',
                select: 'type_of_tree acreage',
            },
            {
                path: 'notice',
                select: 'note',
            }
        ])
        .select(
            "status land_lot diagram_url ipfsHash house_id other_construction_works prod_forest_is_an_artificial_forest perennial_trees notice"
        );

    if (!myAssets || myAssets.length == 0)
      return res.status(200).json({ myAssets: [] });

    // try to get lasted status from Blockchain
    for (let asset of myAssets) {
      try {
        const newStatus = await getLandLicenseStatus(asset._id);
        asset.status = newStatus;
        await asset.save();
      } catch (error) {
        console.log("Internal Server Error", error);
      }
    }
    res.status(200).json({ myAssets: myAssets });
  } catch (error) {
    console.error("Error while get user asset information:", error);
    res.status(500).json({ error: "Error while get user asset information" });
  }
};

export { sendEmailOTP, verifyEmailOTP, getAllUser, putUserKYC, getMyAsset };
