
import User from "../Models/userModel.js";
import { createdCertificate } from '../Utils/certificateService.js';
import { uploadToIPFS, uploadImageToPinata } from '../Utils/IPFSStorageService.js';
import { getRole } from '../Utils/getRole.js'
import { getLandLicenseStatus } from '../Utils/getLandLicense.js'
import LandLicence from "../Models/landLicenceModel.js";

const getListUserApproved = (async (req, res) => {
    try {
        const users = await User.find()
        const filteredUsers = []

        for (const user of users) {
            const role = await getRole(user.wallet_address)
            if (role !== 'ADMIN' && role !== 'NOTARY') {
                // filteredUsers.push(user)
                if(user.verifiedStatus === 'approved'){
                    filteredUsers.push(user);
                }
            }
        }
        res.json({ users: filteredUsers })
    } catch (error) {
        console.log('Get list user failed', error.message);
        res.status(500).json({ message: 'Get list user failed' })
    }
})

const getListUser = (async (req, res) => {
    try {
        const users = await User.find()
        const filteredUsers = []

        for (const user of users) {
            const role = await getRole(user.wallet_address)
            if (role !== 'ADMIN' && role !== 'NOTARY') {
                // filteredUsers.push(user)
                if(user.verifiedStatus === 'submitted KYC'){
                    filteredUsers.push(user);
                }
            }
        }
        res.json({ users: filteredUsers })
    } catch (error) {
        console.log('Get list user failed', error.message);
        res.status(500).json({ message: 'Get list user failed' })
    }
})

const updateStatusUser = (async (req, res) => {
    try {
        const { userId, verifiedStatus } = req.body
        // const userId = req.user._id
        if (req.user.verifiedStatus === 'submitted KYC') {
            await User.findByIdAndUpdate(userId, { verifiedStatus: verifiedStatus })
            if (verifiedStatus === 'approved') return res.status(200).json({ message: 'User KYC successfully' })
            if (verifiedStatus === 'rejected') {
                await User.findByIdAndUpdate(userId, {
                    $unset: {
                        email: '',
                        idCard_back_url: '',
                        idCard_front_url: '',
                        permanent_address: '',
                        hometown: '',
                        birthday: '',
                        id_card: '',
                        full_name: ''
                    },
                    verifiedStatus: 'not verified',
                    verifiedEmail: false,
                });
                return res.status(200).json({ message: 'Invalid information' });
            }
        } else {
            return res.status(400).json({ message: 'The user status is invalid' });
        }
    } catch (error) {
        console.error('Error managing KYC:', error.message);
        return res.status(500).json({ message: 'Internal server error' });
    }
})

const createCertificate = (async (req, res) => {
    try {
        const { userId,
            land_lot,
            house,
            prodForestIsAnArtificialForest,
            otherConstructionWork,
            perennialTrees,
            notice,
            fileImage
        } = req.processedData;

        const ownerUser = await User.findById(userId)
        if (!ownerUser) {
            return res.status(400).json({ error: 'owner not found' })
        }
        if (!land_lot) {
            return res.status(400).json({ error: 'land lot is required' })
        }
        let diagram_url, ipfsHash;
        try {
            if (fileImage && fileImage.path && ownerUser.full_name) {
                diagram_url = await uploadImageToPinata(ownerUser.full_name, fileImage.path);
            }
        } catch (err) {
            console.log('createCertificate ERROR:', err.message);
            return res.status(400).json({ error: 'Failed to upload diagram IMAGE to IPFS Storage' });
        }
        // Create certificate Json and upload to IPFS Storage
        const cerfificateJson = {
            land_lot,
            house,
            prodForestIsAnArtificialForest,
            otherConstructionWork,
            perennialTrees,
            notice,
            diagram_url: `ipfs://${diagram_url}`
        }
        try {
            const dataJson = JSON.parse(JSON.stringify(cerfificateJson))
            ipfsHash = await uploadToIPFS(dataJson, {
                name: ownerUser.full_name
            });
        } catch (err){
            console.log('createCertificate ERROR:', err);
            return res.status(400).json({ error: 'Failed to upload certificate json to IPFS Storage' });
        }
        // STEP 5: Create sub-collection: House, ProdForestIsAnArtificialForest, otherConstructionWork ..
        const data = {
            ownerUser,
            land_lot,
            house,
            prodForestIsAnArtificialForest,
            otherConstructionWork,
            perennialTrees,
            notice,
            diagram_url,
            ipfsHash
        };
        const certificateData = await createdCertificate(data)

        res.status(200).json({
            message: 'Create certificate successed',
            status: certificateData.status,
            ipfsHash,
            landLiceneId: certificateData._id,
            owner: ownerUser.wallet_address,
            diagram_url,
        });

    } catch (error) {
        console.error('Error creating certificate:', error);
        return res.status(500).json({ error: 'Failed to create certificate' });
    }
})


const getAssetById = async (req, res) => {
    try {
        const {assetId} = req.query;
        const asset = await LandLicence.findById(assetId)
        if (!asset) return res.status(404).json({ message: 'Asset not found' })
        try {
            const newStatus = await getLandLicenseStatus(assetId);
            const landLicene = await LandLicence.findByIdAndUpdate(assetId, { $set: { status: newStatus } }, { new: true })
            return res.status(200).json({ message: 'Get status Certificate success', landLicene })
        } catch (error) {
            return res.status(200).json({ message: 'Get status Certificate faild', landLicene })
        }
    } catch (error) {
        console.error('Error updating landLicense: ', error);
        return res.status(500).json({ error: 'Error updating landLicense' });
    }
};


export { updateStatusUser, createCertificate, getListUser, getAssetById, getListUserApproved }

