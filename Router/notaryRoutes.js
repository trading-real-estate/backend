import express from 'express';
import { updateStatusUser, createCertificate, getListUser, getAssetById, getListUserApproved } from '../Controllers/notaryController.js'
import { verifyToken, authNotary } from '../Middleware/authMiddleware.js';
import { handleFormData } from '../Middleware/formDataMiddleware.js'
import multer from 'multer'

const router = express.Router();

const upload = multer({ dest: 'uploads/' });

//verify status User Kyc
router.put('/verifyStatusUserKYC', verifyToken, authNotary, updateStatusUser)
// get list user
router.get('/getListUser', verifyToken, authNotary, getListUser)
// get list approved 
router.get('/getListUserApproved', verifyToken, authNotary, getListUserApproved)
//create Certificate
router.post('/createCertificate', verifyToken, authNotary, upload.single('fileImage'), handleFormData, createCertificate)
//update Certificate
router.get('/getAssetById', verifyToken, authNotary, getAssetById)



export default router