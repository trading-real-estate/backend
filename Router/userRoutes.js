import express from 'express';
import { loginUser, getLoginMessage } from '../Controllers/authController.js';
import { sendEmailOTP, verifyEmailOTP, getAllUser, putUserKYC, getMyAsset } from '../Controllers/userController.js';
import { verifyToken } from '../Middleware/authMiddleware.js';
import { validatePutKYC } from '../Middleware/validateMiddleware.js'

const router = express.Router();

//auth
router.get('/getLoginMessage', getLoginMessage)
router.post('/login', loginUser)
// User KYC
// verify email
router.post('/sendEmail', verifyToken, sendEmailOTP)
router.post('/verifyOTP', verifyToken, verifyEmailOTP)
//Get user info
router.get('/userinfo', verifyToken, getAllUser)
//Put  user Kyc
router.put('/userKYC', verifyToken, validatePutKYC, putUserKYC)
//get my asset 
router.get('/myAsset', verifyToken, getMyAsset)




export default router