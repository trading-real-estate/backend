import express from 'express';
import { verifyToken, authAdmin } from '../Middleware/authMiddleware.js';
import { getListNotary} from '../Controllers/adminController.js'
const router = express.Router();

router.get('/getListNotary',verifyToken, authAdmin, getListNotary)

export default router