import mongoose from 'mongoose';

const landLicenceSchema = new mongoose.Schema({
    userId:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    status:{
        type: String,
        enum: ['DRAFT', 'ONCHAIN_PENDING', 'ONCHAIN_ACTIVE', 'ONCHAIN_IN_TRANSACTION'],
        default: 'none'
    },
    land_lot: { 
        type: mongoose.Schema.Types.ObjectId,
        ref:'LandLot',
    },
    house_id :{
        type: mongoose.Schema.Types.ObjectId,
        ref:'House',
    },
    other_construction_works:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'OtherConstructionWork',
    },
    prod_forest_is_an_artificial_forest: {
        type: mongoose.Schema.Types.ObjectId,
        ref:'ProdForestIsAnArtificialForest',
    },
    perennial_trees:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'PerennialTree',
    },
    notice:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'Notice',
    },
    diagram_url:{
        type: String
    },
    ipfs_hash:{
        type:String
    },
    transaction_hash:{
        type:String
    }
})

const LandLicence = mongoose.model('LandLicense',landLicenceSchema)

export default LandLicence