import mongoose from 'mongoose';

const prodForestIsAnArtificialForestSchema = new mongoose.Schema({
    landLicenceId:{
        type : mongoose.Schema.Types.ObjectId,
        ref : 'LandLicense',
    },
    forest_type: {
        type: String
    },
    acreage_forest:{
        type: String
    }
})
const ProdForestIsAnArtificialForest = mongoose.model('ProdForestIsAnArtificialForest',prodForestIsAnArtificialForestSchema)

export default ProdForestIsAnArtificialForest