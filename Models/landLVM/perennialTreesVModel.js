import mongoose from 'mongoose';

const perennialTreeSchema = new mongoose.Schema({
    landLicenceId:{
        type : mongoose.Schema.Types.ObjectId,
        ref : 'LandLicense',
    },
    type_of_tree:{
        type: String
    },
    acreage:{
        type: Number
    }
})
const PerennialTree = mongoose.model('PerennialTree',perennialTreeSchema)

export default PerennialTree