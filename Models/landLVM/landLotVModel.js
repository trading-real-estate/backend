import mongoose from 'mongoose';

const landLotSchema = new mongoose.Schema({
    landLicenceId:{
        type : mongoose.Schema.Types.ObjectId,
        ref : 'LandLicense',
    },
    land_plot_no:{
        type: Number
    },
    map_sheet_no:{
        type: Number
    },
    address:{
        type: String
    },
    common_land_area:{
        type: Number
    },
    land_area_private:{
        type: Number
    },
    spend_time:{
        type: Number
    },
    purpose_of_using_land:{
        type: String
    },
    origin_of_land_use:{
        type: String
    }
})
const LandLot = mongoose.model('LandLot',landLotSchema)

export default LandLot