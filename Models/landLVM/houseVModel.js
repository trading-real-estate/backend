import mongoose from 'mongoose';

const houseSchema = new mongoose.Schema({
    landLicenceId:{
        type : mongoose.Schema.Types.ObjectId,
        ref : 'LandLicense',
    },
    construction_area:{
        type: Number
    },
    floor_area:{
        type: Number
    },
    level:{
        type: Number
    },
    structure:{
        type: String
    },
    floors_of_number:{
        type: Number
    },
    ownership_period:{
        type: String
    },
    year_complete_construction:{
        type: Date
    },
})
const House = mongoose.model('House',houseSchema)

export default House