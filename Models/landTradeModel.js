import mongoose from 'mongoose';

const landTradeSchema = new mongoose.Schema({
    landLicenceId : {
        type : mongoose.Schema.Types.ObjectId,
        ref:'LandLicence',
        required : true,
    },
    seller_user_id : {
        type : mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required : true,
    },
    buyer_user_id:{
        type : mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required : true,
    },
    amount:{
        type : String,
    },
    deposit_amount:{
        type: String
    },
    payment_amount:{
        type: String
    },
    title:{
        type: String
    },
    descirbe:{
        type: String
    },
    price:{
        type: Number
    },
    number_land:{
        type: String
    },
    number_map:{
        type: String
    },
    address_and:{
        type: String
    },
    time_use:{
        type: String
    },
    Target_uses:{
        type: String
    },
    form_use:{
        type: String
    },
    origin_of_use:{
        type: String
    },
    image_url:{
        type: String
    },
    imagePlot_ap:{
        type: String
    },
    income_ax:{
        type: Number
    },
    time_deposit:{
        type: Number
    },
    remaining_amount:{
        type: Number
    },
    amount_received:{
        type: Number
    },
    comment:{
        type: String
    },
    status:{
        type: String,
        enum: ['listed for sale', 'deposit made', 'deposit confirmed', ' payment successful, transaction completed'],
        default: 'None'
    },
    start_date:{
        type: Date
    },
    end_date:{
        type: Date
    }
})

const LandTrade = mongoose.model('LandTrade',landTradeSchema)

export default LandTrade