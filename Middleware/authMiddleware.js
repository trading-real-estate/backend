import jwt from 'jsonwebtoken'
import User from '../Models/userModel.js'

const verifyToken = async (req, res, next) => {
  try {
    const token = req.header('Authorization');

    if (!token) return res.status(401).json({ message: 'Access denied. No token provided.' });

    if (!token.startsWith('Bearer ')) return res.status(401).json({ message: 'Invalid token format. It should be in the format "Bearer <token>".' });

    const authToken = token.split(' ')[1];
    const decoded = jwt.verify(authToken, process.env.JWT_SECRET)

    req.user = await User.findById(decoded.payload.userId)

    if (!req.user) {
      return res.status(401).json({ message: 'Access denied. User not found.' });
    }
    next()
  } catch (error) {
    console.error('Error in verifyToken middleware:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
}

const authNotary = (async (req, res, next) => {
  try {
    // Kiểm tra xem người dùng có vai trò là Notary hay không
    const decodedToken = jwt.verify(req.headers.authorization.split(' ')[1], process.env.JWT_SECRET);
    if (decodedToken.payload.role !== 'NOTARY') {
      return res.status(401).json({ message: 'Access denied. Not a Notary.' });
    }
    next();
  } catch (error) {
    console.error('Error in authNotary middleware:', error);
    res.status(500).json({ message: 'Not authorized, invalid token and not notary' });
   
  }
})

const authAdmin = (async (req, res, next) => {
  try {
    const decodedToken = jwt.verify(req.headers.authorization.split(' ')[1], process.env.JWT_SECRET);
    if (decodedToken.payload.role !== 'ADMIN') {
      return res.status(401).json({ message: 'Access denied. Not a Admin.' });
    }
    next();
  } catch (error) {
    console.error('Error in authAdmin middleware:', error);
    res.status(500).json({ message: 'Not authorized, invalid token and not Admin' });
  }
})

export { verifyToken, authNotary, authAdmin }