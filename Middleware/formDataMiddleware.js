

const handleFormData = async (req, res, next) => {
    try {
      const { userId, land_lot, house, prodForestIsAnArtificialForest, otherConstructionWork, perennialTrees, notice } = req.body;
      const fileImage = req.file;
      // Create an object with the processed data
      req.processedData = {
        userId,
        land_lot: land_lot && JSON.parse(land_lot),
        house: house && JSON.parse(house),
        otherConstructionWork: otherConstructionWork && JSON.parse(otherConstructionWork),
        prodForestIsAnArtificialForest: prodForestIsAnArtificialForest && JSON.parse(prodForestIsAnArtificialForest),        
        perennialTrees: perennialTrees && JSON.parse(perennialTrees),
        notice: notice && JSON.parse(notice),
        fileImage
      };
      // console.log(req.processedData);
      next();
    } catch (error) {
      console.error('Error processing form data:', error.message);
      res.status(400).json({ error: 'Invalid form data' });
    }
};

export { handleFormData };