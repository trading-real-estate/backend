import { validationResult, body } from "express-validator";

const validateResults = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    next();
}

const validatePutKYC = [
    body("full_name").notEmpty().isString(),
    body("id_card").notEmpty().isString(),
    body("birthday").notEmpty().isDate(),
    body("hometown").notEmpty().isString(),
    body("permanent_address").notEmpty().isString(),
    body("idCard_front_url").notEmpty().isString(),
    body("idCard_back_url").notEmpty().isString(),
    // body('otp').notEmpty().isString().isLength({ min: 6, max: 6 }).matches(/^[0-9]+$/),
    validateResults
];

const checkNumericValue = (value, { req }) => {
    if (parseFloat(value) < 0) {
        throw new Error("Value must be greater than or equal to 100");
    }
    return true;
};

const validateHouseData = [
    body("construction_area").notEmpty().isNumeric().custom(checkNumericValue),
    body("floor_area").notEmpty().isNumeric().custom(checkNumericValue),
    body("level").notEmpty().isNumeric().custom(checkNumericValue),
    body("structure").notEmpty().isString(),
    body("floors_of_number").notEmpty().isNumeric().custom(checkNumericValue),
    body("ownership_period").notEmpty().isString(),
    body("year_complete_construction").notEmpty().isISO8601(),
    validateResults
];
const validateProdForestIsAnArtificialForest = [
    body("forest_type").notEmpty().isString(),
    body("acreage_forest").notEmpty().isString(),
    validateResults
];
const validateOtherConstructionWork = [
    body("type_of_construction").notEmpty().isString(),
    body("project_category").notEmpty().isString(),
    body("construction_area").notEmpty().isNumeric().custom(checkNumericValue),
    body("floor_area").notEmpty().isNumeric().custom(checkNumericValue),
    body("ownership_period").notEmpty().isString(),
    validateResults
];
const validateTree = [
    body("type_of_tree").notEmpty().isString(),
    body("acreage").notEmpty().isNumeric().custom(checkNumericValue),
    validateResults
];
const validateNotice = [
    body("structure").notEmpty().isString(),
    validateResults
];
const validateLandLot = [
    body("land_plot_no").notEmpty().isNumeric().custom(checkNumericValue),
    body("map_sheet_no").notEmpty().isNumeric().custom(checkNumericValue),
    body("address").notEmpty().isString(),
    body("common_land_area").notEmpty().isNumeric().custom(checkNumericValue),
    body("land_area_private").notEmpty().isNumeric().custom(checkNumericValue),
    body("spend_time").notEmpty().isNumeric().custom(checkNumericValue),
    body("purpose_of_using_land").notEmpty().isString(),
    body("origin_of_land_use").notEmpty().isString(),
    validateResults
]

export { validatePutKYC, validateHouseData, validateProdForestIsAnArtificialForest, validateOtherConstructionWork, validateTree, validateNotice, validateLandLot};
