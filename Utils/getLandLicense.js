import Web3 from "web3";
import LandLicence from "../Models/landLicenceModel.js";
import { ABI_Land } from "../ABI.js";
import { LAND_LICENCE_STATUS_CONVERT } from "../contains.js";

const web3 = new Web3(
    // `https://eth-sepolia.g.alchemy.com/v2/${process.env.YOUR_API_KEY_AIchemy}`
    `https://eth-sepolia.g.alchemy.com/v2/5t3The4TctCS9qZ7SL2D8DG3FiwDbpUt`
);
const getLandLicenseStatus = async (licenseId) => {
    try {
        const contract = new web3.eth.Contract(
            ABI_Land,
            process.env.YOUR_CONTRACT_ADDRESS_LANDLICENE
        );
        const status = await contract.methods.getStateOfCert(licenseId).call();
        return LAND_LICENCE_STATUS_CONVERT[status];
    } catch (error) {
        console.error("error get status of landlicenseId :", error);
        throw new Error("error get status of landlicenseId ");
    }
};

const syncAllLandLicenseFromBlockchain = async () => {
    const myAssets = await LandLicence.find();
    console.log('syncAllLandLicenseFromBlockchain START: ', myAssets.length);
    for (let asset of myAssets) {
        try {
            console.log('land id:', asset._id.toString())
            const newStatus = await getLandLicenseStatus(asset._id.toString());
            asset.status = newStatus;
            console.log(asset.status);
            await asset.save();
        } catch (err) {
            console.log("syncAllLandLicenseFromBlockchain Err", err.message);
        }
    }
    console.log('syncAllLandLicenseFromBlockchain FINISH');
}

export { getLandLicenseStatus, syncAllLandLicenseFromBlockchain }