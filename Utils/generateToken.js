import jwt from 'jsonwebtoken'
import User from '../Models/userModel.js'

const generateToken = (userId, role) => {
    const payload = {
        userId: userId,
        role: role,
    };

    const token = jwt.sign({ payload }, process.env.JWT_SECRET, {
        expiresIn: '1d',
    })
    return token
}

export { generateToken }