import Web3 from "web3";
import {ABI} from '../ABI.js';
const getRole = async (address) => {
    try {
        const web3 = new Web3(`https://eth-sepolia.g.alchemy.com/v2/${process.env.YOUR_API_KEY_AIchemy}`);
        const contract = new web3.eth.Contract(ABI, process.env.YOUR_CONTRACT_ADDRESS_ROLE)
        const hasNotaryRole = await contract.methods.hasNotaryRole(address).call();
        const isAdmin = await contract.methods.isAdmin(address).call();

        let role;
        if (hasNotaryRole) {
            role = 'NOTARY';
        } else if (isAdmin) {
            role = 'ADMIN';
        } else {
            role = 'USER'
        }
        return role;
    } catch (error) {
        console.error("Lỗi khi gọi hàm từ smart contract:", error);
        throw new Error("Lỗi khi gọi hàm từ smart contract");
    }
};

export { getRole }