import pkg from '@pinata/sdk';
import fs from 'fs'
const PinataSDK = pkg;
const pinata = new PinataSDK({
    pinataApiKey: 'e7ba3ca1383ab23bab90',
    pinataSecretApiKey: '9ddfc6bd53b9feb3e69b479c4e09055e5fd40d75c0bdbb35b5d97f6d64297dfd',
});

const pinataClient = async () => {
    return pinata;
};

const uploadToIPFS = (async (certificateData,pinataMetadata) => {
    try {
        const pinata = await pinataClient();
        const options = {
            pinataMetadata
        };
        const result = await pinata.pinJSONToIPFS(certificateData, options);
        console.log("uploadToIPFS",result);
        return result.IpfsHash
    } catch (error) {
        console.error('Failed upload file JSON certificate:', error.message);
        throw new Error('Failed upload file JSON certificate');
    }
})

const uploadImageToPinata = async (name, imgPath) => {
    try {
        const pinata = await pinataClient();
        const pinataOptions = {
            pinataMetadata: {
              name: name,
            },
          };
          const readStream = fs.createReadStream(imgPath);
          const result = await pinata.pinFileToIPFS(readStream, pinataOptions);
          console.log("uploadImageToPinata",result);
          fs.unlink(imgPath, (err) => {
            if (err) {
              console.error('Lỗi khi xóa tệp:', err);
            } else {
              console.log('Đã xóa tệp thành công:', imgPath);
            }
          });
          return result.IpfsHash
    } catch (error) {
        console.error('Lỗi khi tải lên ảnh lên Pinata:', error);
        throw new Error('Lỗi khi tải lên ảnh lên Pinata');
    }
}

export { uploadToIPFS,uploadImageToPinata }