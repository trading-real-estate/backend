import House from "../Models/landLVM/houseVModel.js";
import Notice from "../Models/landLVM/noticeVModel.js";
import ProdForestIsAnArtificialForest from "../Models/landLVM/prodForestIsAnArtificialForestVModel.js";
import PerennialTree from "../Models/landLVM/perennialTreesVModel.js";
import OtherConstructionWork from "../Models/landLVM/otherConstructionWorkVModel.js";
import LandLicence from "../Models/landLicenceModel.js";
import LandLot from '../Models/landLVM/landLotVModel.js';
import { LAND_LICENCE_STATUS_CONVERT } from "../contains.js";

const createHouse = async (landLiceneId, data) => {
  const {
    construction_area,
    floor_area,
    level,
    structure,
    floors_of_number,
    ownership_period,
    year_complete_construction,
  } = data;
  const houseInstance = new House({
    landLiceneId,
    construction_area,
    floor_area,
    level,
    structure,
    floors_of_number,
    ownership_period,
    year_complete_construction,
  });
  await houseInstance.save();
  return houseInstance;
};

const createProdForestIsAnArtificialForest = async (landLiceneId, data) => {
  const { forest_type, acreage_forest  } = data;

  const prodInstance = new ProdForestIsAnArtificialForest({
    landLiceneId,
    forest_type,
    acreage_forest,
  });

  await prodInstance.save();
  return prodInstance;
};

const createOtherConstructionWork = async (landLiceneId, data) => {
  const {
    type_of_construction,
    project_category,
    construction_area,
    floor_area,
    ownership_period,
  } = data;

  const otherInstance = new OtherConstructionWork({
    landLiceneId,
    type_of_construction,
    project_category,
    construction_area,
    floor_area,
    ownership_period,
  });

  await otherInstance.save();
  return otherInstance;
};

const createPerennialTree = async (landLiceneId, data) => {
  const { type_of_tree, acreage } = data;

  const perennialInstance = new PerennialTree({
    landLiceneId,
    type_of_tree,
    acreage,
  });

  await perennialInstance.save();
  return perennialInstance;
};

const createNotice = async (landLiceneId, data) => {
  const { note } = data;

  const noteInstance = new Notice({
    landLiceneId,
    note,
  });

  await noteInstance.save();
  return noteInstance;
};

const createLandLot = async (landLiceneId, data) => {
  const {
    land_plot_no,
    map_sheet_no,
    address,
    common_land_area,
    land_area_private,
    spend_time,
    purpose_of_using_land,
    origin_of_land_use,
  } = data;

  const landLotInstance = new LandLot({
    landLiceneId,
    land_plot_no,
    map_sheet_no,
    address,
    common_land_area,
    land_area_private,
    spend_time,
    purpose_of_using_land,
    origin_of_land_use,
  });

  await landLotInstance.save();
  return landLotInstance;
};

const createdCertificate = async (data) => {
  const {
    ownerUser,
    land_lot,
    house,
    prodForestIsAnArtificialForest,
    otherConstructionWork,
    perennialTrees,
    notice,
    diagram_url,
    ipfsHash,
  } = data;
  const newLandLicene = new LandLicence({
    userId: ownerUser._id,
    status: LAND_LICENCE_STATUS_CONVERT.DRAFT,
    ipfsHash: ipfsHash,
    diagram_url: diagram_url,
  });
  let housObject,
    prodForestIsAnArtificialForestObj,
    otherConstructionWorkObj,
    perennialTreeObj,
    noticeObj,
    landLotObj;

  if (house) {
    housObject = await createHouse(newLandLicene._id, house);
  }
  if (prodForestIsAnArtificialForest) {
    prodForestIsAnArtificialForestObj =
      await createProdForestIsAnArtificialForest(
        newLandLicene._id,
        prodForestIsAnArtificialForest
      );
  }
  if (otherConstructionWork) {
    otherConstructionWorkObj = await createOtherConstructionWork(
      newLandLicene._id,
      otherConstructionWork
    );
  }
  if (perennialTrees) {
    perennialTreeObj = await createPerennialTree(
      newLandLicene._id,
      perennialTrees
    );
  }
  if (notice) {
    noticeObj = await createNotice(newLandLicene._id, notice);
  }

  if (land_lot){
    landLotObj = await createLandLot(newLandLicene._id, land_lot);
  }

  newLandLicene.land_lot = landLotObj && landLotObj._id;
  newLandLicene.house_id = housObject && housObject._id;
  newLandLicene.other_construction_works =
    otherConstructionWorkObj && otherConstructionWorkObj._id;
  newLandLicene.prod_forest_is_an_artificial_forest =
    prodForestIsAnArtificialForestObj && prodForestIsAnArtificialForestObj._id;
  newLandLicene.perennial_trees = perennialTreeObj && perennialTreeObj._id;
  newLandLicene.notice = noticeObj && noticeObj._id;
  return await newLandLicene.save();
};

export { createdCertificate };
