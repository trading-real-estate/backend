import express from 'express';
import colors from "colors"
import dotenv from "dotenv"
import connectDB from './Config/db.js';
import userRoutes from './Router/userRoutes.js';
import notaryRoutes from './Router/notaryRoutes.js';
import adminRoutes from './Router/adminRoutes.js'
import searchRoutes from './Router/searchRoutes.js';
import morgan from 'morgan';
import { syncLandLicenceJob } from './Jobs/sync-landlicence.job.js';
import cors from 'cors'

// configura env
dotenv.config()

//rest object
const app = express();

//connect database
connectDB();
app.use(cors())
app.use(express.json({ limit: '1000mb' }))
app.use(express.urlencoded({ limit: '1000mb', extended: true }))
app.use(morgan('dev'))
// app.use(bodyParser.json({limit: '1000mb'}))
// app.use(bodyParser.urlencoded({limit: '1000mb', extended : true }))
// app.use(cookieParser())
//rest api
app.use('/api/users', userRoutes)
app.use('/api/search', searchRoutes)
app.use('/api/notary', notaryRoutes)
app.use('/api/admin', adminRoutes)

app.get('/api', (req, res) => {
    res.send({ message: "wellcom to  ecommerce app" })
})

// START WORKER
// require('./Jobs/sync-landlicence.job.js').syncLandLicenceJob.start();   
await syncLandLicenceJob.start();


//PORT
const PORT = process.env.PORT || 8080

//run listen
app.listen(PORT, () => {
    console.log(`server running on  ${process.env.DEV_MODE} mode on port: ${PORT}`.bgCyan.white)
})

